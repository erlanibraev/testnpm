"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var pageabl_model_1 = require("./pageabl.model");
var PageableComponent = (function () {
    function PageableComponent() {
        this.nextPage = new core_1.EventEmitter();
        this.prevPage = new core_1.EventEmitter();
    }
    PageableComponent.prototype.ngOnInit = function () {
    };
    PageableComponent.prototype.nextPageClick = function () {
        var nextPage = this.pageable;
        nextPage.number = (nextPage.number < nextPage.totalPages - 1 ? nextPage.number + 1 : nextPage.number);
        this.nextPage.emit(nextPage);
    };
    PageableComponent.prototype.prevPageClick = function () {
        var prevPage = this.pageable;
        prevPage.number = (prevPage.number > 0 ? prevPage.number - 1 : prevPage.number);
        this.prevPage.emit(prevPage);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', pageabl_model_1.Pageable)
    ], PageableComponent.prototype, "pageable", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PageableComponent.prototype, "nextPage", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PageableComponent.prototype, "prevPage", void 0);
    PageableComponent = __decorate([
        core_1.Component({
            selector: 'my-pageable',
            templateUrl: "app/pageable/pageable.component.html",
            styleUrls: ["app/pageable/pageable.component.css"]
        }), 
        __metadata('design:paramtypes', [])
    ], PageableComponent);
    return PageableComponent;
}());
exports.PageableComponent = PageableComponent;
exports.maxPageNumbers = 7;
//# sourceMappingURL=pageable.component.js.map