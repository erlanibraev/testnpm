import {Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import {Pageable, Page} from "./pageabl.model";

@Component({
    selector: 'my-pageable',
    templateUrl:"app/pageable/pageable.component.html",
    styleUrls: ["app/pageable/pageable.component.css"]
})
export class PageableComponent implements OnInit {
    @Input() pageable: Pageable;
    @Output()public nextPage = new EventEmitter<Pageable>();
    @Output()public prevPage = new EventEmitter<Pageable>();
    pageNumbers: number[];

    ngOnInit() {

    }

    nextPageClick() {
        let nextPage : Pageable = this.pageable;
        nextPage.number = (nextPage.number < nextPage.totalPages - 1 ? nextPage.number+1: nextPage.number);
        this.nextPage.emit(nextPage);
    }

    prevPageClick() {
        let prevPage : Pageable = this.pageable;
        prevPage.number = (prevPage.number > 0 ? prevPage.number - 1 : prevPage.number);
        this.prevPage.emit(prevPage);
    }

}

export const maxPageNumbers : number = 7;
