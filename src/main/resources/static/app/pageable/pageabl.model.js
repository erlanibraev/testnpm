"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Sort = (function () {
    function Sort() {
    }
    return Sort;
}());
exports.Sort = Sort;
var Pageable = (function () {
    function Pageable() {
    }
    return Pageable;
}());
exports.Pageable = Pageable;
var Page = (function (_super) {
    __extends(Page, _super);
    function Page() {
        _super.apply(this, arguments);
    }
    return Page;
}(Pageable));
exports.Page = Page;
;
//# sourceMappingURL=pageabl.model.js.map