export class Sort {
    direction: string;
    property: string;
    ignoreCase: boolean;
    nullHandling: string;
    ascending:boolean;
}

export class Pageable {
    number: number;
    last: boolean;
    totalPages: number;
    totalElements: number;
    size: number;
    first: boolean;
    numberOfElements: number;
    sort: Sort;
}

export class Page<T> extends Pageable {
    content: T[];
};

