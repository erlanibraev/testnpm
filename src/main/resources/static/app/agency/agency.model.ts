import {Page} from "../pageable/pageabl.model";

export const DATE_BEGIN = new Date('1900-01-01');
export const DATE_END = new Date('2999-01-01');

export class Agency {
    id: number;
    codA: string;
    parent: number;
    descriptionKz: string;
    descriptionEn: string;
    descriptionRu: string;
    cmtKz: string;
    cmtEn: string;
    cmtRu: string;
};

export class AgencyVer {
    id: number;
    codCS: string;
    nameKz: string;
    nameEn: string;
    nameRu: string;
    fullNameKz: string;
    fullNameEn: string;
    fullNameRu: string;
    descriptionKz: string;
    descriptionEn: string;
    descriptionRu: string;
    cmtKz: string;
    cmtEn: string;
    cmtRu: string;
    versionNumber:string;
    dateBegin:Date;
    dateEnd:Date;
    agency:Agency;
};

export class AgencyProp {
    id: number;
    address: string;
    phone: string;
    fioHead: string;
    fioResponsible: string;
    agencyVer: AgencyVer;
};

export class AgencyVerPage extends Page<AgencyVer> {

}
