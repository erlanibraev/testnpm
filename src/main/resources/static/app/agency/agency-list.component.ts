import {AgencyVer, AgencyVerPage} from './agency.model';
import {Component, OnInit} from '@angular/core';
import {AgencyService} from './agency.service'
import {Router} from '@angular/router';
import {Pageable, Page} from "../pageable/pageabl.model";

@Component({
    selector: 'my-agencylist',
    templateUrl: 'app/agency/agency-list.component.html',
    styleUrls: ['app/agency/agency-list.component.css', 'app/buttons.css']
})
export class AgencyListComponent  implements OnInit {
    agencyList:AgencyVer[];
    myPage: Pageable;
    title = 'Агенства';
    selectedAgency: AgencyVer;

    constructor(private agencyService: AgencyService, private router: Router) {
    }

    ngOnInit() {
        this.getAgency()
    }

    getAgency() {
        this.agencyService.getAgencyVerPage(this.myPage)
            .then(agencyList => {
                this.myPage = agencyList;
                this.agencyList = agencyList.content;
            });
    }

    onSelect(agency:AgencyVer) {
        this.selectedAgency = agency;
        this.router.navigate(['/agencyverlist', agency.agency.codA]);
    }

    addNew() {
        this.router.navigate(['/agencyform']);
    }

    onNextPage(nextPage:Pageable) {
        this.myPage = nextPage;
        this.getAgency();
    }

    onPrevPage(prevPage:Pageable) {
        this.myPage = prevPage;
        this.getAgency();
    }


}