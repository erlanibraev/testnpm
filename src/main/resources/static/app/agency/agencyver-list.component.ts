import {AgencyVer, Agency, AgencyVerPage} from './agency.model';
import {Component, OnInit} from '@angular/core';
import {AgencyService} from './agency.service'
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Pageable} from "../pageable/pageabl.model";

@Component({
    selector: 'my-agencyverlist',
    templateUrl: 'app/agency/agency-list.component.html',
    styleUrls: ['app/agency/agency-list.component.css', 'app/buttons.css']
})
export class AgencyVerListComonent {
    agencyList: AgencyVer[];
    myPage: AgencyVerPage;
    title = 'Версии агенства';
    selectedAgencyVer: AgencyVer;
    error: any;
    navigated = false;
    codA: string;

    constructor(private agencyService: AgencyService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            if (params['id'] !== undefined) {
                this.codA = params['id'];
                this.navigated = true;
                this.getAhencyVer();
            } else {
                this.navigated = false;
                this.agencyList = [];
            }
        });
    }

    getAhencyVer() {
        this.agencyService.getAgencyVerByAgencyPage(this.codA, this.myPage)
            .then(agencyList => {
                this.agencyList = agencyList.content;
                this.myPage = agencyList;
            });
    }

    onSelect(agencyVer:AgencyVer) {
        this.selectedAgencyVer = agencyVer;
        this.router.navigate(['/agencyform', agencyVer.agency.codA, agencyVer.id]);
    }

    addNew() {
        this.router.navigate(['/agencyform', this.codA]);
    }

    onNextPage(nextPage:Pageable) {

    }

    onPrevPage(prevPage:Pageable) {

    }


}