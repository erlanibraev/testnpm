import {AgencyVer, Agency, DATE_BEGIN, DATE_END} from './agency.model';
import {Component, OnInit, Input} from '@angular/core';
import {AgencyService} from './agency.service'
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'my-agencyform',
    templateUrl: "app/agency/agency-form.component.html",
    styleUrls:["app/agency/agency-form.component.css"]
})
export class AgencyFormComponent implements OnInit {

    agencyVer: AgencyVer;
    agency: Agency;

    constructor(private agencyService: AgencyService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            if (params['id'] !== undefined) {
                let id = +params['id'];
                this.agencyService.getAgencyVer(id)
                    .then(agencyVer => {
                        this.agencyVer = agencyVer;
                        this.agency = agencyVer.agency
                    });
            } else if (params['agencyId'] !== undefined) {
                let id : string = params['agencyId'];
                this.agencyService.getAgency(id).then(agency=> {
                    this.agency = agency;
                    this.agencyVer = this.newAgencyVer(agency);
                });
            } else {
                this.agency = new Agency();
                this.agencyService.addAgency(this.agency).then(agency => {
                    this.agency = agency;
                    this.agencyVer = this.newAgencyVer(agency);
                });
            }
        });
    }

    onSelDateBegin(date: Date) {
        this.agencyVer.dateBegin = date;
    }

    onSelDateEnd(date: Date) {
        this.agencyVer.dateEnd = date;
    }

    saveAgency() {
        this.agencyService.saveAgency(this.agencyVer.agency)
            .then(agency => {
                this.agency = agency;
                this.agencyService.saveAgencyVer(this.agencyVer)
                .then(this.goBack)
            });
    }

    newAgencyVer(agency:Agency): AgencyVer {
        let agencyVer = new AgencyVer();
        agencyVer.agency = agency;
        agencyVer.dateBegin = DATE_BEGIN;
        agencyVer.dateEnd = DATE_END;
        return agencyVer;
    }

    goBack(): void {
        window.history.back();
    }
}