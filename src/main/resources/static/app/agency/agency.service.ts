import {Injectable} from '@angular/core'
import {Agency, AgencyVer, AgencyVerPage} from './agency.model'
import 'rxjs/add/operator/toPromise';
import {Headers, Http, URLSearchParams} from "@angular/http";
import {Pageable} from "../pageable/pageabl.model";

@Injectable()
export class AgencyService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private restUrl = 'http://192.168.1.249:8080/rest/agency';  // URL to web api

    constructor(private http: Http) {

    }


    getAgencyVerPage(page:Pageable): Promise<AgencyVerPage> {
        let params = new URLSearchParams();
        if (page !== undefined && page.number !== undefined) {
            params.set("page", page.number.toString());
        }
        return this.http.get(this.restUrl+'/agencyver', {
            search: params
        })
            .toPromise()
            .then(response => (response.json() as AgencyVerPage))
            .catch(this.handleError);
    }

    getAgencyVerByAgencyPage(codA:string, page:Pageable): Promise<AgencyVerPage> {
        let params = new URLSearchParams();
        if (page !== undefined && page.number !== undefined) {
            params.set("page", page.number.toString());
        }
        let restUrl = this.restUrl+"/"+codA+"/agencyver";
        return this.http.get(restUrl, {
            search: params
        })
            .toPromise()
            .then(response => (response.json() as AgencyVerPage))
            .catch(this.handleError);
    }

    getAgencyList(): Promise<AgencyVer[]> {
        return this.http.get(this.restUrl+'/agencyver')
            .toPromise()
            .then(response => (response.json() as AgencyVerPage).content)
            .catch(this.handleError);
    }

    getAgencyVerByAgency(codA:string): Promise<AgencyVer[]> {
        let restUrl = this.restUrl+"/"+codA+"/agencyver";
        return this.http.get(restUrl)
            .toPromise()
            .then(response => (response.json() as AgencyVerPage).content)
            .catch(this.handleError);
    }

    getAgencyVer(id:number): Promise<AgencyVer> {
        let restUrl = this.restUrl+"/agencyver/"+id;
        return this.http.get(restUrl)
            .toPromise()
            .then(response => response.json() as AgencyVer)
            .catch(this.handleError);
    }

    getAgency(codA: string): Promise<Agency> {
        let restUrl = this.restUrl+"/"+codA;
        return this.http.get(restUrl)
            .toPromise()
            .then(response => response.json() as Agency)
            .catch(this.handleError);
    }

    addAgency(agency:Agency):Promise<Agency> {
        return this.http.post(this.restUrl,agency)
            .toPromise()
            .then(response => response.json() as Agency)
            .catch(this.handleError);
    }

    addAgencyVer(agencyVer:AgencyVer):Promise<AgencyVer> {
        return this.http.post(this.restUrl+"/"+agencyVer.agency.codA+"/agencyver",agencyVer)
            .toPromise()
            .then(response => response.json() as AgencyVer)
            .catch(this.handleError);
    }

    saveAgency(agency:Agency): Promise<Agency> {
        let restUrl = this.restUrl;
        if (agency.id !== undefined) {
            restUrl += "/"+agency.codA;
        }
        return this.http.post(restUrl,agency)
            .toPromise()
            .then(response => response.json() as Agency)
            .catch(this.handleError);

    }

    saveAgencyVer(agencyVer:AgencyVer):Promise<AgencyVer> {
        let restUrl = this.restUrl+"/"+agencyVer.agency.codA+"/agencyver";
        if (agencyVer.id !== undefined) {
            restUrl += "/"+agencyVer.id;
        }

        return this.http.post(restUrl,agencyVer)
            .toPromise()
            .then(response => response.json() as AgencyVer)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}