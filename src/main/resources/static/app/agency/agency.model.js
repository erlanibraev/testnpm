"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var pageabl_model_1 = require("../pageable/pageabl.model");
exports.DATE_BEGIN = new Date('1900-01-01');
exports.DATE_END = new Date('2999-01-01');
var Agency = (function () {
    function Agency() {
    }
    return Agency;
}());
exports.Agency = Agency;
;
var AgencyVer = (function () {
    function AgencyVer() {
    }
    return AgencyVer;
}());
exports.AgencyVer = AgencyVer;
;
var AgencyProp = (function () {
    function AgencyProp() {
    }
    return AgencyProp;
}());
exports.AgencyProp = AgencyProp;
;
var AgencyVerPage = (function (_super) {
    __extends(AgencyVerPage, _super);
    function AgencyVerPage() {
        _super.apply(this, arguments);
    }
    return AgencyVerPage;
}(pageabl_model_1.Page));
exports.AgencyVerPage = AgencyVerPage;
//# sourceMappingURL=agency.model.js.map