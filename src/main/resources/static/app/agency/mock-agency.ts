import {Agency, AgencyVer} from './agency.model';



export const AGENCY: Agency[] = [
    {
        id: 1,
        codA: '_A_1',
        parent: 0,
        descriptionRu: '',
        descriptionEn: '',
        descriptionKz: '',
        cmtRu: '',
        cmtEn: '',
        cmtKz: '',
    }, {
        id: 2,
        codA: '_A_2',
        parent: 0,
        descriptionRu: '',
        descriptionEn: '',
        descriptionKz: '',
        cmtRu: '',
        cmtEn: '',
        cmtKz: '',
    }, {
        id: 3,
        codA: '_A_3',
        parent: 0,
        descriptionRu: '',
        descriptionEn: '',
        descriptionKz: '',
        cmtRu: '',
        cmtEn: '',
        cmtKz: '',
    }

]

export const AGENCY_VER: AgencyVer[] = [
    {
        id: 1,
        nameRu: 'Агенство 1',
        nameEn: 'Agency 1',
        nameKz: '',
        fullNameRu: 'Агенство 1',
        fullNameEn: 'Agency 1',
        fullNameKz: '',
        codCS: '',
        descriptionRu: '',
        descriptionEn: '',
        descriptionKz: '',
        cmtRu: '',
        cmtEn: '',
        cmtKz: '',
        dateBegin: new Date('1900-01-01'),
        dateEnd: new Date('2012-12-31'),
        versionNumber: '',
        agency: AGENCY[0]
    }, {
        id: 2,
        codCS: '',
        nameKz: '',
        nameEn: 'Agency 2',
        nameRu: 'Агенство 2',
        fullNameKz: '',
        fullNameEn: 'Agency 2',
        fullNameRu: 'Агенство 2',
        descriptionKz: '',
        descriptionEn: '',
        descriptionRu: '',
        cmtKz: '',
        cmtEn: '',
        cmtRu: '',
        versionNumber: '',
        dateBegin: new Date('1900-01-01'),
        dateEnd: new Date('2999-01-01'),
        agency: AGENCY[1]
    }, {
        id: 3,
        codCS: '',
        nameKz: '',
        nameEn: 'Agency 3',
        nameRu: 'Агенство 3',
        fullNameKz: '',
        fullNameEn: 'Agency 3',
        fullNameRu: 'Агенство 3',
        descriptionKz: '',
        descriptionEn: '',
        descriptionRu: '',
        cmtKz: '',
        cmtEn: '',
        cmtRu: '',
        versionNumber: '',
        dateBegin: new Date('1900-01-01'),
        dateEnd: new Date('2999-01-01'),
        agency: AGENCY[2]
    }, {
        id: 4,
        nameRu: 'Агенство 1',
        nameEn: 'Agency 1',
        nameKz: '',
        fullNameRu: 'Агенство 1',
        fullNameEn: 'Agency 1',
        fullNameKz: '',
        codCS: '',
        descriptionRu: '',
        descriptionEn: '',
        descriptionKz: '',
        cmtRu: '',
        cmtEn: '',
        cmtKz: '',
        dateBegin: new Date('2013-01-01'),
        dateEnd: new Date('2999-01-01'),
        versionNumber: '',
        agency: AGENCY[0]
    }
];