"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var agency_model_1 = require('./agency.model');
var core_1 = require('@angular/core');
var agency_service_1 = require('./agency.service');
var router_1 = require('@angular/router');
var AgencyFormComponent = (function () {
    function AgencyFormComponent(agencyService, route) {
        this.agencyService = agencyService;
        this.route = route;
    }
    AgencyFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            if (params['id'] !== undefined) {
                var id = +params['id'];
                _this.agencyService.getAgencyVer(id)
                    .then(function (agencyVer) {
                    _this.agencyVer = agencyVer;
                    _this.agency = agencyVer.agency;
                });
            }
            else if (params['agencyId'] !== undefined) {
                var id = params['agencyId'];
                _this.agencyService.getAgency(id).then(function (agency) {
                    _this.agency = agency;
                    _this.agencyVer = _this.newAgencyVer(agency);
                });
            }
            else {
                _this.agency = new agency_model_1.Agency();
                _this.agencyService.addAgency(_this.agency).then(function (agency) {
                    _this.agency = agency;
                    _this.agencyVer = _this.newAgencyVer(agency);
                });
            }
        });
    };
    AgencyFormComponent.prototype.onSelDateBegin = function (date) {
        this.agencyVer.dateBegin = date;
    };
    AgencyFormComponent.prototype.onSelDateEnd = function (date) {
        this.agencyVer.dateEnd = date;
    };
    AgencyFormComponent.prototype.saveAgency = function () {
        var _this = this;
        this.agencyService.saveAgency(this.agencyVer.agency)
            .then(function (agency) {
            _this.agency = agency;
            _this.agencyService.saveAgencyVer(_this.agencyVer)
                .then(_this.goBack);
        });
    };
    AgencyFormComponent.prototype.newAgencyVer = function (agency) {
        var agencyVer = new agency_model_1.AgencyVer();
        agencyVer.agency = agency;
        agencyVer.dateBegin = agency_model_1.DATE_BEGIN;
        agencyVer.dateEnd = agency_model_1.DATE_END;
        return agencyVer;
    };
    AgencyFormComponent.prototype.goBack = function () {
        window.history.back();
    };
    AgencyFormComponent = __decorate([
        core_1.Component({
            selector: 'my-agencyform',
            templateUrl: "app/agency/agency-form.component.html",
            styleUrls: ["app/agency/agency-form.component.css"]
        }), 
        __metadata('design:paramtypes', [agency_service_1.AgencyService, router_1.ActivatedRoute])
    ], AgencyFormComponent);
    return AgencyFormComponent;
}());
exports.AgencyFormComponent = AgencyFormComponent;
//# sourceMappingURL=agency-form.component.js.map