import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {AgencyListComponent} from './agency/agency-list.component'
import {AgencyVerListComonent} from "./agency/agencyver-list.component";
import {AgencyFormComponent} from "./agency/agency-form.component";
import {SourceStockComponent} from "./sourcestock/sourcestock.component";
import {SourceStockFormComponent} from "./sourcestock/sourcestock-from.component";

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }, {
        path: 'home',
        component: HomeComponent
    }, {
        path: 'agencylist',
        component: AgencyListComponent
    }, {
        path: 'sourcestock',
        component: SourceStockComponent
    }, {
        path: 'agencyverlist/:id',
        component: AgencyVerListComonent
    }, {
        path: 'agencyform/:agencyId/:id',
        component: AgencyFormComponent
    }, {
        path: 'agencyform/:agencyId',
        component: AgencyFormComponent
    }, {
        path: 'agencyform',
        component: AgencyFormComponent
    }, {
        path: 'sourcestockform/:codSS',
        component: SourceStockFormComponent
    }, {
        path: 'sourcestockform',
        component: SourceStockFormComponent
    }
];

export const routing = RouterModule.forRoot(appRoutes);