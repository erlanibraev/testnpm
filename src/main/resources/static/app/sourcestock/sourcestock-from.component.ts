import {Component, OnInit} from "@angular/core";
import {SourceStock, DATE_END, DATE_BEGIN} from "./sourcestock.model";
import {SourceStockService} from "./sourcestock.service";
import {ActivatedRoute, Params} from "@angular/router";
import {Agency, AgencyVer} from "../agency/agency.model";
import {AgencyService} from "../agency/agency.service";

@Component({
    selector:"sourcestockformcomponent",
    templateUrl: "app/sourcestock/sourcestock-form.component.html"
})
export class SourceStockFormComponent implements OnInit {

    sourcestock: SourceStock;
    agencyList: AgencyVer[];
    selectedAgency: string;

    constructor(private sourcestockService: SourceStockService, private agencyService: AgencyService, private route: ActivatedRoute) {

    }

    ngOnInit() {
        this.agencyService
            .getAgencyList()
            .then( agencyList => this.agencyList = agencyList);

        this.route.params.forEach((
            (params: Params) => {
                if (params['codSS'] !== undefined) {
                    let codSS:string = params['codSS'];
                    this.sourcestockService.getSourceStockByCodSS(codSS)
                        .then(sourcestock => {
                            this.sourcestock = sourcestock;
                            this.selectedAgency = sourcestock.agency.codA;
                        });
                } else {
                    this.sourcestock = this.newSourceStock();
                }
            })
        );
    }

    saveSourcestock() {
        this.sourcestockService.saveSourcestock(this.sourcestock)
            .then(sourcestock => {
                this.sourcestock = sourcestock;
                this.goBack();
            });
    }

    goBack(): void {
        window.history.back();
    }

    onSelDateBegin(date: Date) {
        this.sourcestock.dBeg = date;
    }

    onSelDateEnd(date: Date) {
        this.sourcestock.dEnd = date;
    }

    newSourceStock(): SourceStock {
        let sourcestock = new SourceStock();
        sourcestock.dBeg = DATE_BEGIN;
        sourcestock.dEnd = DATE_END;
        return sourcestock;
    }

    onChangeAgnecy(codA:string) {
        this.agencyService
            .getAgency(codA)
            .then(agency => this.sourcestock.agency = agency);
    }

}