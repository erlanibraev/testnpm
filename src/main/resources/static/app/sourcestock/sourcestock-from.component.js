"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var sourcestock_model_1 = require("./sourcestock.model");
var sourcestock_service_1 = require("./sourcestock.service");
var router_1 = require("@angular/router");
var agency_service_1 = require("../agency/agency.service");
var SourceStockFormComponent = (function () {
    function SourceStockFormComponent(sourcestockService, agencyService, route) {
        this.sourcestockService = sourcestockService;
        this.agencyService = agencyService;
        this.route = route;
    }
    SourceStockFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.agencyService
            .getAgencyList()
            .then(function (agencyList) { return _this.agencyList = agencyList; });
        this.route.params.forEach((function (params) {
            if (params['codSS'] !== undefined) {
                var codSS = params['codSS'];
                _this.sourcestockService.getSourceStockByCodSS(codSS)
                    .then(function (sourcestock) {
                    _this.sourcestock = sourcestock;
                    _this.selectedAgency = sourcestock.agency.codA;
                });
            }
            else {
                _this.sourcestock = _this.newSourceStock();
            }
        }));
    };
    SourceStockFormComponent.prototype.saveSourcestock = function () {
        var _this = this;
        this.sourcestockService.saveSourcestock(this.sourcestock)
            .then(function (sourcestock) {
            _this.sourcestock = sourcestock;
            _this.goBack();
        });
    };
    SourceStockFormComponent.prototype.goBack = function () {
        window.history.back();
    };
    SourceStockFormComponent.prototype.onSelDateBegin = function (date) {
        this.sourcestock.dBeg = date;
    };
    SourceStockFormComponent.prototype.onSelDateEnd = function (date) {
        this.sourcestock.dEnd = date;
    };
    SourceStockFormComponent.prototype.newSourceStock = function () {
        var sourcestock = new sourcestock_model_1.SourceStock();
        sourcestock.dBeg = sourcestock_model_1.DATE_BEGIN;
        sourcestock.dEnd = sourcestock_model_1.DATE_END;
        return sourcestock;
    };
    SourceStockFormComponent.prototype.onChangeAgnecy = function (codA) {
        var _this = this;
        this.agencyService
            .getAgency(codA)
            .then(function (agency) { return _this.sourcestock.agency = agency; });
    };
    SourceStockFormComponent = __decorate([
        core_1.Component({
            selector: "sourcestockformcomponent",
            templateUrl: "app/sourcestock/sourcestock-form.component.html"
        }), 
        __metadata('design:paramtypes', [sourcestock_service_1.SourceStockService, agency_service_1.AgencyService, router_1.ActivatedRoute])
    ], SourceStockFormComponent);
    return SourceStockFormComponent;
}());
exports.SourceStockFormComponent = SourceStockFormComponent;
//# sourceMappingURL=sourcestock-from.component.js.map