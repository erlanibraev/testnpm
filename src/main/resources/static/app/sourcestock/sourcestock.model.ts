import {Agency} from "../agency/agency.model";
import {Page} from "../pageable/pageabl.model";

export const DATE_BEGIN = new Date('1900-01-01');
export const DATE_END = new Date('2999-01-01');

export class SourceStock {
    public id : number;
    public codSS: string;
    public nameKz: string;
    public nameRu: string;
    public nameEn: string;
    public fullNameKz: string;
    public fullNameRu: string;
    public fullNameEn: string;
    public parent: number;
    public sourceStockType: number;
    public dBeg: Date;
    public dEnd: Date;
    public ord: number;
    public connectionURL: string;
    public usrLogin: string;
    public usrPassword: string;
    public cmtKz: string;
    public cmtRu: string;
    public cmtEn: string;
    public descriptionKz: string;
    public descriptionRu: string;
    public descriptionEn: string;
    public agency: Agency;

}

export class SourceStockPage extends Page<SourceStock> {

}

export class SourceStockFilter {
    codA:string;
    name:string;
}