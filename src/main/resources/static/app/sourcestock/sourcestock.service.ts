import {Injectable} from '@angular/core'
import 'rxjs/add/operator/toPromise';
import {Headers, Http, URLSearchParams} from "@angular/http";
import {Pageable} from "../pageable/pageabl.model";
import {SourceStockPage, SourceStock, SourceStockFilter} from "./sourcestock.model";

@Injectable()
export class SourceStockService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private restUrl = 'http://192.168.1.249:8080/rest/sourcestock';  // URL to web api

    constructor(private http: Http) {

    }

    getSourceStockPage(page:Pageable, filters: SourceStockFilter): Promise<SourceStockPage> {
        let params = new URLSearchParams();
        if (page !== undefined) {
            if (page.number !== undefined) {
                params.set("page", page.number.toString());
            }
        }
        if (filters !== undefined) {
            if (filters.codA !== undefined) {
                params.set("codA", filters.codA);
            }
            if (filters.name !== undefined) {
                params.set("name", filters.name);
            }
        }
        return this.http.get(this.restUrl, {
            search: params
        })
            .toPromise()
            .then(response => (response.json() as SourceStockPage))
            .catch(this.handleError);
    }

    getSourceStockByCodSS(codSS:string): Promise<SourceStock> {
        let restUrl = this.restUrl+"/"+codSS;
        return this.http.get(restUrl)
            .toPromise()
            .then(response => response.json() as SourceStock)
            .catch(this.handleError);
    }

    saveSourcestock(sourcestock:SourceStock): Promise<SourceStock> {
        let restUrl = this.restUrl;
        if (sourcestock.id !== undefined) {
            restUrl += "/"+sourcestock.codSS;
        }
        return this.http.post(restUrl,sourcestock)
            .toPromise()
            .then(response => response.json() as SourceStock)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}