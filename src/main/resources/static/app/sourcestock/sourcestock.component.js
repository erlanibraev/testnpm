"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var sourcestock_model_1 = require("./sourcestock.model");
var sourcestock_service_1 = require("./sourcestock.service");
var router_1 = require("@angular/router");
var agency_service_1 = require("../agency/agency.service");
var SourceStockComponent = (function () {
    function SourceStockComponent(sourcestockService, agencyService, router) {
        this.sourcestockService = sourcestockService;
        this.agencyService = agencyService;
        this.router = router;
        this.title = "Источники данных";
        this.filters = new sourcestock_model_1.SourceStockFilter();
    }
    SourceStockComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.agencyService
            .getAgencyList()
            .then(function (agencyList) { return _this.agencyList = agencyList; });
        this.getSourcestock();
    };
    SourceStockComponent.prototype.getSourcestock = function () {
        var _this = this;
        this.sourcestockService.getSourceStockPage(this.myPage, this.filters)
            .then(function (sourcestockList) {
            _this.myPage = sourcestockList;
            _this.sourcestockList = sourcestockList.content;
        });
    };
    SourceStockComponent.prototype.onSelect = function (sourcestock) {
        this.selectedSourcestock = sourcestock;
        this.router.navigate(['/sourcestockform', this.selectedSourcestock.codSS]);
    };
    SourceStockComponent.prototype.addNew = function () {
        this.router.navigate(['/sourcestockform']);
    };
    SourceStockComponent.prototype.onNextPage = function (nextPage) {
        this.myPage = nextPage;
        this.getSourcestock();
    };
    SourceStockComponent.prototype.onPrevPage = function (prevPage) {
        this.myPage = prevPage;
        this.getSourcestock();
    };
    SourceStockComponent.prototype.onChangeAgency = function (codA) {
        if (codA !== 'all') {
            this.filters.codA = codA;
        }
        else {
            this.filters.codA = undefined;
        }
        this.getSourcestock();
    };
    SourceStockComponent.prototype.onChangeName = function (name) {
        this.filters.name = name;
        this.getSourcestock();
    };
    SourceStockComponent = __decorate([
        core_1.Component({
            selector: "sourcestockpage",
            templateUrl: 'app/sourcestock/sourcestock.component.html',
            styleUrls: ['app/sourcestock/sourcestock.component.css', 'app/buttons.css']
        }), 
        __metadata('design:paramtypes', [sourcestock_service_1.SourceStockService, agency_service_1.AgencyService, router_1.Router])
    ], SourceStockComponent);
    return SourceStockComponent;
}());
exports.SourceStockComponent = SourceStockComponent;
//# sourceMappingURL=sourcestock.component.js.map