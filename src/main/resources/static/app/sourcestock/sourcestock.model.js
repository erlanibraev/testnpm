"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var pageabl_model_1 = require("../pageable/pageabl.model");
exports.DATE_BEGIN = new Date('1900-01-01');
exports.DATE_END = new Date('2999-01-01');
var SourceStock = (function () {
    function SourceStock() {
    }
    return SourceStock;
}());
exports.SourceStock = SourceStock;
var SourceStockPage = (function (_super) {
    __extends(SourceStockPage, _super);
    function SourceStockPage() {
        _super.apply(this, arguments);
    }
    return SourceStockPage;
}(pageabl_model_1.Page));
exports.SourceStockPage = SourceStockPage;
var SourceStockFilter = (function () {
    function SourceStockFilter() {
    }
    return SourceStockFilter;
}());
exports.SourceStockFilter = SourceStockFilter;
//# sourceMappingURL=sourcestock.model.js.map