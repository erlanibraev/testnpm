import {Component, OnInit} from "@angular/core";
import {SourceStock, SourceStockFilter} from "./sourcestock.model";
import {Pageable} from "../pageable/pageabl.model";
import {SourceStockService} from "./sourcestock.service";
import {Router} from "@angular/router";
import {Agency, AgencyVer} from "../agency/agency.model";
import {AgencyService} from "../agency/agency.service";

@Component({
    selector: "sourcestockpage",
    templateUrl: 'app/sourcestock/sourcestock.component.html',
    styleUrls: ['app/sourcestock/sourcestock.component.css', 'app/buttons.css']
})
export class SourceStockComponent implements OnInit {
    sourcestockList: SourceStock[];
    myPage: Pageable;
    title: string="Источники данных";
    selectedSourcestock: SourceStock;
    selectAgency:string;
    agencyList:AgencyVer[];
    filters: SourceStockFilter = new SourceStockFilter();

    constructor(private sourcestockService: SourceStockService, private agencyService: AgencyService, private router: Router) {
    }

    ngOnInit() {
        this.agencyService
            .getAgencyList()
            .then( agencyList => this.agencyList = agencyList);
        this.getSourcestock()
    }

    getSourcestock() {
        this.sourcestockService.getSourceStockPage(this.myPage, this.filters)
            .then(sourcestockList => {
                this.myPage = sourcestockList;
                this.sourcestockList = sourcestockList.content;
            });
    }

    onSelect(sourcestock:SourceStock) {
        this.selectedSourcestock = sourcestock;
        this.router.navigate(['/sourcestockform', this.selectedSourcestock.codSS]);
    }

    addNew() {
        this.router.navigate(['/sourcestockform']);
    }

    onNextPage(nextPage:Pageable) {
        this.myPage = nextPage;
        this.getSourcestock();
    }

    onPrevPage(prevPage:Pageable) {
        this.myPage = prevPage;
        this.getSourcestock();
    }

    onChangeAgency(codA:string) {
        if (codA !== 'all') {
            this.filters.codA = codA;
        } else {
            this.filters.codA = undefined;
        }
        this.getSourcestock();
    }

    onChangeName(name:string) {
        this.filters.name = name;
        this.getSourcestock();
    }

}