"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/toPromise');
var http_1 = require("@angular/http");
var SourceStockService = (function () {
    function SourceStockService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.restUrl = 'http://192.168.1.249:8080/rest/sourcestock'; // URL to web api
    }
    SourceStockService.prototype.getSourceStockPage = function (page, filters) {
        var params = new http_1.URLSearchParams();
        if (page !== undefined) {
            if (page.number !== undefined) {
                params.set("page", page.number.toString());
            }
        }
        if (filters !== undefined) {
            if (filters.codA !== undefined) {
                params.set("codA", filters.codA);
            }
            if (filters.name !== undefined) {
                params.set("name", filters.name);
            }
        }
        return this.http.get(this.restUrl, {
            search: params
        })
            .toPromise()
            .then(function (response) { return (response.json()); })
            .catch(this.handleError);
    };
    SourceStockService.prototype.getSourceStockByCodSS = function (codSS) {
        var restUrl = this.restUrl + "/" + codSS;
        return this.http.get(restUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SourceStockService.prototype.saveSourcestock = function (sourcestock) {
        var restUrl = this.restUrl;
        if (sourcestock.id !== undefined) {
            restUrl += "/" + sourcestock.codSS;
        }
        return this.http.post(restUrl, sourcestock)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    SourceStockService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    SourceStockService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SourceStockService);
    return SourceStockService;
}());
exports.SourceStockService = SourceStockService;
//# sourceMappingURL=sourcestock.service.js.map