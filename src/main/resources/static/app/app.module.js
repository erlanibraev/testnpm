"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var app_component_1 = require('./app.component');
var app_routing_1 = require('./app.routing');
var platform_browser_1 = require('@angular/platform-browser');
var home_component_1 = require("./home/home.component");
var agency_service_1 = require('./agency/agency.service');
var forms_1 = require("@angular/forms");
var datepicker_1 = require("./datepickerinput/datepicker");
var datepicker_input_component_1 = require("./datepickerinput/datepicker-input.component");
var http_1 = require('@angular/http');
var pageable_component_1 = require("./pageable/pageable.component");
var sourcestock_service_1 = require("./sourcestock/sourcestock.service");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                datepicker_1.DatePickerComponent,
                datepicker_input_component_1.DatepickerInputComponent,
                pageable_component_1.PageableComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_1.routing,
                forms_1.FormsModule,
                http_1.HttpModule
            ],
            providers: [
                agency_service_1.AgencyService,
                sourcestock_service_1.SourceStockService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map