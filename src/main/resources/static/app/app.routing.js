"use strict";
var router_1 = require('@angular/router');
var home_component_1 = require('./home/home.component');
var agency_list_component_1 = require('./agency/agency-list.component');
var agencyver_list_component_1 = require("./agency/agencyver-list.component");
var agency_form_component_1 = require("./agency/agency-form.component");
var sourcestock_component_1 = require("./sourcestock/sourcestock.component");
var sourcestock_from_component_1 = require("./sourcestock/sourcestock-from.component");
var appRoutes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }, {
        path: 'home',
        component: home_component_1.HomeComponent
    }, {
        path: 'agencylist',
        component: agency_list_component_1.AgencyListComponent
    }, {
        path: 'sourcestock',
        component: sourcestock_component_1.SourceStockComponent
    }, {
        path: 'agencyverlist/:id',
        component: agencyver_list_component_1.AgencyVerListComonent
    }, {
        path: 'agencyform/:agencyId/:id',
        component: agency_form_component_1.AgencyFormComponent
    }, {
        path: 'agencyform/:agencyId',
        component: agency_form_component_1.AgencyFormComponent
    }, {
        path: 'agencyform',
        component: agency_form_component_1.AgencyFormComponent
    }, {
        path: 'sourcestockform/:codSS',
        component: sourcestock_from_component_1.SourceStockFormComponent
    }, {
        path: 'sourcestockform',
        component: sourcestock_from_component_1.SourceStockFormComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map