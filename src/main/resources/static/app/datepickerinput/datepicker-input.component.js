"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DatepickerInputComponent = (function () {
    function DatepickerInputComponent() {
        this.minDate = '01.01.1900';
        this.maxDate = '31.12.2999';
        this.disableDays = []; //For Sunday and Saturday
        this.toContainPrevMonth = false;
        this.toContainNextMonth = false;
        this.value = '';
        this.returnDate = new core_1.EventEmitter();
    }
    DatepickerInputComponent.prototype.ngOnChanges = function () {
        if (this.selDate !== null && this.selDate !== undefined) {
        }
    };
    /* setInputDate(event:any) {
       this.value = event.target.value;
     }*/
    DatepickerInputComponent.prototype.setDate = function (date) {
        this.selDate = moment(date, 'DD.MM.YYYY').toDate();
        this.returnDate.emit(this.selDate);
    };
    Object.defineProperty(DatepickerInputComponent.prototype, "selDate", {
        get: function () {
            return moment(this._selDate, 'DD.MM.YYYY').toDate();
        },
        set: function (date) {
            this._selDate = moment(date).format('DD.MM.YYYY', true);
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], DatepickerInputComponent.prototype, "returnDate", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Date), 
        __metadata('design:paramtypes', [Date])
    ], DatepickerInputComponent.prototype, "selDate", null);
    DatepickerInputComponent = __decorate([
        core_1.Component({
            selector: 'datepicker-input',
            template: "\n<div *ngIf=\"selDate\">\n  <input #dateText type='text' [(ngModel)]=\"_selDate\"/>\n  <date-picker \n              [value]=\"value\"\n              [minDate]=\"minDate\" \n              [maxDate]=\"maxDate\"\n              [disableDays]=\"disableDays\"\n              [toContainPrevMonth]=\"toContainPrevMonth\"\n              [toContainNextMonth]=\"toContainNextMonth\"\n              (selectedDate)='setDate($event)'></date-picker>\n</div>\n"
        }), 
        __metadata('design:paramtypes', [])
    ], DatepickerInputComponent);
    return DatepickerInputComponent;
}());
exports.DatepickerInputComponent = DatepickerInputComponent;
//# sourceMappingURL=datepicker-input.component.js.map