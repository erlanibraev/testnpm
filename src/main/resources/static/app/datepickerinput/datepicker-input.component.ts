import {Component, Input, Output, OnChanges, EventEmitter} from '@angular/core';
import Any = jasmine.Any;
declare var moment:any;

@Component({
  selector: 'datepicker-input',
  template: `
<div *ngIf="selDate">
  <input #dateText type='text' [(ngModel)]="_selDate"/>
  <date-picker 
              [value]="value"
              [minDate]="minDate" 
              [maxDate]="maxDate"
              [disableDays]="disableDays"
              [toContainPrevMonth]="toContainPrevMonth"
              [toContainNextMonth]="toContainNextMonth"
              (selectedDate)='setDate($event)'></date-picker>
</div>
`
})
export class DatepickerInputComponent implements OnChanges {
  private _selDate:string;
  private minDate:string='01.01.1900';
  private maxDate:string='31.12.2999';
  private disableDays:Array<number>=[];    //For Sunday and Saturday
  private toContainPrevMonth:boolean = false;
  private toContainNextMonth:boolean = false;
  private value:string='';
  @Output() public returnDate = new EventEmitter<Date>();

  ngOnChanges() {
    if (this.selDate !== null && this.selDate !== undefined) {
      // this.selDate = moment(this.editDate).format('DD.MM.YYYY', true);
    }
  }

 /* setInputDate(event:any) {
    this.value = event.target.value;
  }*/
  setDate(date:String){
    this.selDate = moment(date,'DD.MM.YYYY').toDate();
    this.returnDate.emit(this.selDate);
  }

  @Input()
  public set selDate(date:Date) {
    this._selDate = moment(date).format('DD.MM.YYYY', true);
  }

  public get selDate(): Date {
    return moment(this._selDate,'DD.MM.YYYY').toDate()
  }
}
