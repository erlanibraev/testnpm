import {Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
    selector:'my-home',
    templateUrl: 'app/home/home.component.html',
})
export class HomeComponent {

    constructor(private router: Router) {

    }
}