import {NgModule} from '@angular/core'
import {AppComponent} from './app.component'
import { routing }        from './app.routing';
import { BrowserModule }  from '@angular/platform-browser';
import {HomeComponent} from "./home/home.component";
import {AgencyService} from './agency/agency.service'
import {FormsModule} from "@angular/forms";
import {DatePickerComponent} from "./datepickerinput/datepicker";
import {DatepickerInputComponent} from "./datepickerinput/datepicker-input.component";
import { HttpModule } from '@angular/http';
import {PageableComponent} from "./pageable/pageable.component";
import {SourceStockComponent} from "./sourcestock/sourcestock.component";
import {SourceStockService} from "./sourcestock/sourcestock.service";


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        DatePickerComponent,
        DatepickerInputComponent,
        PageableComponent
    ],
    imports: [
        BrowserModule,
        routing,
        FormsModule,
        HttpModule
    ],
    providers: [
        AgencyService,
        SourceStockService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}