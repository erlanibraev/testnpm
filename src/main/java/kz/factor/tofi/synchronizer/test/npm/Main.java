package kz.factor.tofi.synchronizer.test.npm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by mad on 18.08.16.
 */
@SpringBootApplication
@EnableWebMvc
public class Main {

    public static void main(String... args) {
        SpringApplication.run(Main.class, args);
    }
}
