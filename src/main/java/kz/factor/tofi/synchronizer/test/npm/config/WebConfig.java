package kz.factor.tofi.synchronizer.test.npm.config;

import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by mad on 15.08.16.
 */
@Configuration
@ComponentScan
@EnableWebMvc
public class WebConfig extends WebMvcAutoConfiguration {

}


